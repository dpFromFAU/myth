contract Gasless {
    address lastContributor;

    function() public payable {
        lastContributor = msg.sender;
    }
}
