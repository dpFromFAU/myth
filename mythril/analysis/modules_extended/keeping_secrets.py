from mythril.analysis.report import Issue
import re


# improvements:
# instruction list for contact deployment - similar remix.ethereum.org -->
# PUSH* [...] SSTORE @line of secret
def analyze_secrets(contract):
    """
    Executes analysis module for integer underflow and integer overflow
    :param files: SolidityContracts[] to analyse
    :return: Found issues
    """
    issues = []

    for fileId, file in enumerate(contract.solidity_files):

        searchPattern = re.compile(build_search_pattern(), re.IGNORECASE)

        comments = find_comments(file.data)

        lines = file.data.split('\n')
        offset = 0
        for lineId, line in enumerate(lines):
            for match in re.finditer(searchPattern, line):
                if not is_match_in_comment(match, offset, comments):
                    lineNr = lineId + 1
                    code = line
                    # generiere issue
                    description = "It seems that there is secret information in this contract. (keyword "+ match.group(1) +")\n" + \
                              "Be aware, that the contract will be deployed to the blockchain and secrets inside it can be publicly accessed."
                    # retrieve functioname and pc address
                    pc = None
                    functionName = "n/a"
                    for mappingId, mapping in enumerate(contract.mappings):
                        if mapping.solidity_file_idx == fileId and mapping.lineno == lineNr:
                            offset = mapping.offset
                            length = mapping.length

                            code = file.data[offset:offset + length]
                            pc = mappingId
                            instruction = contract.disassembly.instruction_list[mappingId]
                            break
                            #print(code + ": " + instruction["opcode"])
                    issue = Issue(contract.name, functionName, pc,
                              "Secret information in contract",
                              "Informational",
                              description)
                    issue.filename = file.filename
                    #issue.lineno = file.data[:match.start()].count('\n') + 1
                    issue.lineno = lineNr
                    issue.code = code

                    issues.append(issue)
            offset += len(line) + 1

    return issues


def find_comments(source):
    comments = []
    commentPattern = re.compile(build_comment_pattern())
    for match in re.finditer(commentPattern, source):
        comments.append((match.start(), match.end()))
    return comments


def is_match_in_comment(match, offset, comments):
    for comment in comments:
        if match.start() + offset > comment[0] and match.start() + offset < comment[1]:
            return True
    return False


def build_comment_pattern():
    multiline = '(\/\*[\s\S]*?\*\/)'
    singleline = '\/\/.*\n'
    return multiline + "|" + singleline


def build_search_pattern():
    vars = ["privatekey", "secret", "password"]

    pattern = '('
    for var in vars:
        pattern += var + '|'

    return pattern[:-1] + '){1}\w*\s*='
    # vars = ["privatekey", "secret", "password"]
    #
    # pattern = ''
    # for var in vars:
    #     pattern += var + '|'
    #
    # return pattern[:-1]

# (\w*(secret|password|privatekey)\w*)\s*[;|=]{1}
