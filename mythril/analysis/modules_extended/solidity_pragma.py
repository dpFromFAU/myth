from mythril.analysis.report import Issue
import re


def analyze_pragma(contract):
    """
    Executes analysis module for missing compiler requirement inside of source files
    :param files: SolidityContracts[] to analyse
    :return: Found issues
    """
    issues = []

    files = contract.solidity_files

    for file in files:
        if not (re.search('^\s*pragma solidity \^[\d\.]*;', file.data)):
            filename = file.filename
            description = "It seems that this source file (" + filename.split('/')[-1] + ") does not specify a required compiler version. " + \
                          "Compilation with old compiler versions is a huge security risk. " + \
                          "Please consider adding\n" + \
                          "pragma solidity ^<up-to-date-solc-version>;"
            issue = Issue("n/a", "n/a", 0, "Missing compiler pragma", "Warning", description)
            issue.filename = filename
            issue.lineno = 1

            issues.append(issue)

    return issues
