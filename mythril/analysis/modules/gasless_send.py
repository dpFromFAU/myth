import logging
import sys
import time

from mythril.analysis.report import Issue

'''
MODULE DESCRIPTION:

Tests whether the fallback function is consuming more than 2300 gas.

'''

defaultAmountOfFallbackGas = 2300


def execute(statespace):
    logging.debug("Executing module: GASLESS_SEND")

    issues = []
    # start = time.perf_counter()

    startingGas = 0
    lowestGas = sys.maxsize
    node = None
    address = 0

    # NO REAL PERFORMANCE WIN; STABILITY DRASTICALLY REDUCED BECAUSE OF REDUCE
    # nodesOfFallbackFunction = list(filter(lambda x: x.function_name == "fallback()", statespace.nodes.values()))
    # list(map(lambda x: x.function_name, statespace.nodes.values()))
    # node = nodesOfFallbackFunction[0]
    #
    # from functools import reduce
    # concatedStateList = reduce(lambda x, y: x + y, map(lambda x: x.states, nodesOfFallbackFunction))
    #
    # lowestGas = min(map(lambda z: z.mstate.gas, concatedStateList))
    # startingGas = max(map(lambda z: z.mstate.gas, concatedStateList))
    #
    # stopState = reduce(lambda y1, y2: y1 if (y1.mstate.gas > y2.mstate.gas) else y2,
    #                    filter(lambda x: x.environment.code.instruction_list[x.mstate.pc]['opcode'] == 'STOP',
    #                           concatedStateList
    #                           )
    #                    )
    # address = stopState.environment.code.instruction_list[stopState.mstate.pc - 1]['address']

    for nodeId in statespace.nodes:
        # We're only interested in the fallback function
        if ("fallback" == statespace.nodes[nodeId].function_name):
            node = statespace.nodes[nodeId]

            # start of fallback fct
            if startingGas == 0:
                startingGas = node.states[0].mstate.gas

            # print(node.start_addr + " " + node.states[0].mstate.gas)
            # if node.flags.name:
            #    print(node.flags.name)
            # print(node.flags) # NodeFlags.FUNC_ENTRY

            for state in node.states:
                if state.environment.code.instruction_list[state.mstate.pc]['opcode'] == 'STOP':
                    address = state.environment.code.instruction_list[state.mstate.pc - 1]['address']
                if state.mstate.gas < lowestGas:
                    lowestGas = state.mstate.gas

    # end = time.perf_counter()
    # print(str(end - start) + "sekunden")
    if defaultAmountOfFallbackGas < (startingGas - lowestGas):
        issue = Issue(node.contract_name, "fallback()", address, "Gasless send",
                      "Informational")

        issue.description = "A possible depletion of gas exists in the `fallback` function.\n" \
                            "The function may consumes up to " + str(startingGas - lowestGas) + \
                            ", which is more than the default amount of " + str(
            defaultAmountOfFallbackGas) + " gas for fallback functions."

        issues.append(issue)

    return issues
