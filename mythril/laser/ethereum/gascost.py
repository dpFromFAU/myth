gascost = {
    'PUSH': 3,  # G_verylow
    'DUP': 3,  # G_verylow
    'SWAP': 3,  # G_verylow
    'STOP': 0,  # G_zero
    'ADD': 3,  # G_verylow
    'MUL': 5,  # G_low
	'SUB': 3,  # G_verylow
    'DIV': 5,  # G_low
    'SDIV': 5,  # G_low
    'MOD': 5,  # G_low
    'SMOD': 5,  # G_low
    'ADDMOD': 8,  # G_mid
    'MULMOD': 8,  # G_mid
    'EXP': 10,  # G_high
    'SIGNEXTEND': 5,  # G_low
    'LT': 3,  # G_verylow
    'GT': 3,  # G_verylow
    'SLT': 3,  # G_verylow
    'SGT': 3,  # G_verylow
    'EQ': 3,  # G_verylow
    'ISZERO': 3,  # G_verylow
    'AND': 3,  # G_verylow
    'OR': 3,  # G_verylow
    'XOR': 3,  # G_verylow
    'NOT': 3,  # G_verylow
    'BYTE': 3,  # G_verylow
    'SHA3': 30,  # G_sha3
    'ADDRESS': 2,  # G_base
    'BALANCE': 400,  # G_balance
    'ORIGIN': 2,  # G_base
    'CALLER': 2,  # G_base
    'CALLVALUE': 2,  # G_base
    'CALLDATALOAD': 3,  # G_verylow
    'CALLDATASIZE': 2,  # G_base
    'CALLDATACOPY': 3,  # G_verylow
    'CODESIZE': 2,  # G_base
    'CODECOPY': 3,  # G_verylow
    'GASPRICE': 2,  # G_base
    'EXTCODESIZE': 700,  # W extcode
    'EXTCODECOPY': 700,  # W extcode
    'BLOCKHASH': 20,  # G_blockhash
    'COINBASE': 2,  # G_base
    'TIMESTAMP': 2,  # G_base
    'NUMBER': 2,  # G_base
    'DIFFICULTY': 2,  # G_base
    'GASLIMIT': 2,  # G_base
    'POP': 2,  # G_base
    'MLOAD': 3,  # G_verylow
    'MSTORE': 3,  # G_verylow
    'MSTORE8': 3,  # G_verylow
    'SLOAD': 200,  # G_sload
    'SSTORE': 0,  # 20000 G_sset == non zero set        5000 G_sreset == zero set
    'JUMP': 8,  # G_mid
    'JUMPI': 10,  #   # G_high
    'PC': 2,  # G_base
    'MSIZE': 2,  # G_base
    'GAS': 2,  # G_base
    'JUMPDEST': 1,  # G_jumpdest
    'LOG0': 375,              # 0 topics
    'LOG1': 375 + 1*375,      # 1 topics
    'LOG2': 375 + 2*375,      # 2 topics
    'LOG3': 375 + 3*375,      # 3 topics
    'LOG4': 375 + 4*375,      # 4 topics
    'CREATE': 32000,  # G_create
    # G_codedeposit 200    Paid per byte for a CREATE operation to succeed in placing code into state   TODO
    'CALL': 700,  # G_call
    # G_callvalue 9000    Paid for a non-zero value transfer as part of the CALL operation.   TODO yellowpaper page 29
    # G_stipend     2300    A Stipend for the called contract subtracted from G_callvalue for a non-zero value transfer.   TODO
    'CALLCODE': 700,  # G_call
    'DELEGATECALL': 700,  # G_call
    'CALLBLACKBOX': 700,  # G_call
    'STATICCALL': 700,  # G_call
    'RETURN': 0,  # G_zero
    'RETURNDATASIZE': 2,  # G_base
    'RETURNDATACOPY': 3, # G_verylow
    'REVERT': 0,  # G_zero
    'SUICIDE': 5000,  # G_selfdestruct
}


# TODO https://ethereum.github.io/yellowpaper/paper.pdf page 25ff

# R_selfdestruct    24000   Refund given (added into refund counter) for self-destructing account.
    # R_sclear          15000   Refund given (added into refund counter) when the storage value is set to zero from non-zero
    # changes A_r, Substate, that is acted upon immediatley following to transaction (S.7 Yellowpaper)
# G_newaccount      25000   paid for a CALL or SELFDESTRUCT operation which creates an account.
# G_memory          3       paid for every additional word when expanding memory
# G_txcreate          32000   paid by all contract-creating transactions after the homestead transition
# G_txdatazero      4       paid for every zero byte of data or code for a transaction
# G_txdatanonzero   68      paid for every non-zero byte of data or code for a transaction
# G_transaction     21000   paid for every transaction
# G_quaddivisor    100     the quadratic coefficient of the input sizes of the exponentiation-over-modulo precompiled contract.

SHA3WORD_FEE = 6    # Paid for each word (rounded up) for input data to a SHA3 operation. (one word is 32byte)
COPY_FEE = 3        # Partial payment for *COPY operations, multiplied by words copied, rounded up
EXPBYTE_FEE = 50    # Partial payment when multiplied by log_256(exponent) for the EXP operation
LOGDATA_FEE = 8     # Paid for each byte in a LOG operation’s data.
SSET_FEE = 20000    # Paid for SSTORE when setting from zero to non-zero value
SRESET_FEE = 5000   # Paid for SSTORE otherwise than SSET_FEE
